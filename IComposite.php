<?php

interface IComposite{
	public function add(IComposite $item);	
	public function remove($index);
	public function draw();	
}

