<?php

abstract class AComposite implements IComposite{
	protected $items = [];
	
	public function add(IComposite $item){
		$this->items[] = $item;		
	}
	
	public function remove($index){
		if(isset($this->items[$index])){
			unset($this->items[$index]);
		}
	}
	
	public function getChild($index){
		if(isset($this->items[$index])){
			return $this->items[$index];
		}
		return NULL;
	}
	
	abstract public function draw();	
}