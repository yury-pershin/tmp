var Node = function (name) {
    this.children = [];
    this.name = name;
}
 
Node.prototype = {
    add: function (child) {
        this.children.push(child);
    },
 
    remove: function (child) {
        var length = this.children.length;
        for (var i = 0; i < length; i++) {
            if (this.children[i] === child) {
                this.children.splice(i, 1);
                return;
            }
        }
    },
 
    getChild: function (i) {
        return this.children[i];
    },
 
    hasChildren: function () {
        return this.children.length > 0;
    }
}
 
 
function traverse(indent, node) {
    log.add(Array(indent++).join("--") + node.name);
 
    for (var i = 0, len = node.children.length; i < len; i++) {
        traverse(indent, node.getChild(i));
    }
}
 
var log = (function () {
    var log = "";
 
    return {
        add: function (msg) { log += msg + "\n"; },
        show: function () { console.log(log); log = ""; }
    }
})();
 
function test() {
    var root = new Node("root");
    var sub1 = new Node("sub1")
    var sub2 = new Node("sub2");
    var child_sub11 = new Node("child_sub11");
    var child_sub12 = new Node("child_sub12");
    var child_sub21 = new Node("child_sub21");
    var child_sub22 = new Node("child_sub22");
 
    root.add(sub1);
    root.add(sub2);        
 
    sub1.add(child_sub11);
    sub1.add(child_sub12);
 
    sub2.add(child_sub21);
    sub2.add(child_sub22);
 
    traverse(1, root);
 
    log.show();
}