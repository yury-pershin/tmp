<?php

class Div extends AComposite{
	protected $text;
	protected $doc;	
	
	public function __construct($text){
		$this->text = $text;		
	}
	
	public function draw(){
		$this->doc = '<div>';
		
		$this->doc .= $this->text;
		
		foreach($this->items as $item){
			$this->doc .= $item->draw();			
		}		
				
		$this->doc .= '</div>';
		
		return $this->doc;
	}
}